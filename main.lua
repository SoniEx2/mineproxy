local net = require 'net'
local pp = require 'pretty-print'
local print = print
local error = error
local tinsert = table.insert

local server_addr = os.getenv("SERVER_ADDRESS")
local server_port = tonumber(os.getenv("SERVER_PORT"))
local port = tonumber(os.getenv("LISTEN_PORT"))

local DUMMY = function() end

local DEBUG = false

local function clientHandler(client)
  client:pause()
  local server
  local queued = {}
  client:nodelay(true)
  print("Client connected")

  client:on('error', function(err)
      print("Client read error: " .. err)
      if server then server:shutdown(DUMMY) end
    end)

  client:on('data', function(data)
      if DEBUG then print("CTS", pp.dump(data)) end
      if queued then if DEBUG then print("BROKEN") end tinsert(queued, data) end
      server:write(data)
    end)

  client:on('end', function()
      print("Client disconnected")
      server:shutdown(DUMMY)
    end)

  server = net.createConnection(server_port, server_addr, function(err)
      if err then client:close() --[[TODO propagate]] return end
      server:nodelay(true)
      print("Connected to server")
      
      server:on('error', function(err)
          print("Server read error: " .. err)
          client:shutdown(DUMMY)
        end)

      server:on('data', function(data)
          if DEBUG then print("STC", pp.dump(data)) end
          client:write(data)
        end)
      
      server:on('end', function()
          print("Server disconnected")
          client:shutdown(DUMMY)
        end)
      
      local queue = queued
      queued = nil
      client:resume()
      for i,v in ipairs(queue) do
        if DEBUG then print("QCTS", pp.dump(v)) end
        server:write(v)
      end
    end)
end

local proxy = net.createServer(clientHandler)

proxy:on('error', function(e)
    if e then error(e) end
  end)

proxy:listen(port, function() print("Proxy started") end)